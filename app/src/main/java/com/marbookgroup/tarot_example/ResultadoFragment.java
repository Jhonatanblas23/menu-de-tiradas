package com.marbookgroup.tarot_example;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;

public class ResultadoFragment extends Fragment {

    private View view;
    private RecyclerView myRecycler;
    private AdapterItemResult adapterItemResult;

    private ArrayList<Bitmap> arrayListCards_selected;
    private ArrayList<String> names_cards_selected;

    public ResultadoFragment(ArrayList<Bitmap> arrayListCards_selected, ArrayList<String> names_cards_selected) {
        this.arrayListCards_selected = arrayListCards_selected;
        this.names_cards_selected = names_cards_selected;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_resultado, container, false);

        myRecycler = view.findViewById(R.id.recyclerView_result);
        myRecycler.setLayoutManager(new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false));
        adapterItemResult = new AdapterItemResult(arrayListCards_selected,names_cards_selected);
        myRecycler.setAdapter(adapterItemResult);

        home();

        return view;
    }

    private void home(){

        ImageView imageView = view.findViewById(R.id.house);
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity().getApplication(), MainActivity.class);
                startActivity(intent);
            }
        });
    }


}
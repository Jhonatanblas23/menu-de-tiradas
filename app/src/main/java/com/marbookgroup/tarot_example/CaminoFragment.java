package com.marbookgroup.tarot_example;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class CaminoFragment extends Fragment implements View.OnDragListener, View.OnLongClickListener {

    private View view_;
    private static final String IMAGE_VIEW_TAG = "LAUNCHER LOGO";

    private ImageView selected;
    private int card = 1;
    private ArrayList<Bitmap> arrayListCards;
    private ArrayList<String> names_cards;

    private ArrayList<Bitmap> arrayListCards_selected;
    private ArrayList<String> names_cards_selected;

    private int aleatorio;

    private LinearLayout content_card_1, content_card_2,content_card_3,content_card_4,content_card_5;
    private ImageView card_1,card_2,card_3,card_4,card_5,card_6,card_7,card_8,card_9,card_10
            ,card_11,card_12,card_13,card_14,card_15,card_16,card_17,card_18,card_19,card_20,card_21,card_22;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view_ =  inflater.inflate(R.layout.fragment_camino, container, false);
        arrayListCards = new ArrayList<>();
        names_cards = new ArrayList<>();
        arrayListCards_selected = new ArrayList<>();
        names_cards_selected = new ArrayList<>();
        generate_arrayListCards();
        back();
        card();
        button_next();
        return view_;
    }

    private void back(){
        ImageView back = view_.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void card(){

        card_1 = view_.findViewById(R.id.card_1);
        card_1.setTag(IMAGE_VIEW_TAG);
        card_1.setOnLongClickListener(this);

        card_2 = view_.findViewById(R.id.card_2);
        card_2.setTag(IMAGE_VIEW_TAG);
        card_2.setOnLongClickListener(this);

        card_3 = view_.findViewById(R.id.card_3);
        card_3.setTag(IMAGE_VIEW_TAG);
        card_3.setOnLongClickListener(this);

        card_4 = view_.findViewById(R.id.card_4);
        card_4.setTag(IMAGE_VIEW_TAG);
        card_4.setOnLongClickListener(this);

        card_5 = view_.findViewById(R.id.card_5);
        card_5.setTag(IMAGE_VIEW_TAG);
        card_5.setOnLongClickListener(this);

        card_6 = view_.findViewById(R.id.card_6);
        card_6.setTag(IMAGE_VIEW_TAG);
        card_6.setOnLongClickListener(this);

        card_7 = view_.findViewById(R.id.card_7);
        card_7.setTag(IMAGE_VIEW_TAG);
        card_7.setOnLongClickListener(this);

        card_8 = view_.findViewById(R.id.card_8);
        card_8.setTag(IMAGE_VIEW_TAG);
        card_8.setOnLongClickListener(this);

        card_9 = view_.findViewById(R.id.card_9);
        card_9.setTag(IMAGE_VIEW_TAG);
        card_9.setOnLongClickListener(this);

        card_10 = view_.findViewById(R.id.card_10);
        card_10.setTag(IMAGE_VIEW_TAG);
        card_10.setOnLongClickListener(this);

        card_11 = view_.findViewById(R.id.card_11);
        card_11.setTag(IMAGE_VIEW_TAG);
        card_11.setOnLongClickListener(this);

        card_12 = view_.findViewById(R.id.card_12);
        card_12.setTag(IMAGE_VIEW_TAG);
        card_12.setOnLongClickListener(this);

        card_13 = view_.findViewById(R.id.card_13);
        card_13.setTag(IMAGE_VIEW_TAG);
        card_13.setOnLongClickListener(this);

        card_14 = view_.findViewById(R.id.card_14);
        card_14.setTag(IMAGE_VIEW_TAG);
        card_14.setOnLongClickListener(this);

        card_15 = view_.findViewById(R.id.card_15);
        card_15.setTag(IMAGE_VIEW_TAG);
        card_15.setOnLongClickListener(this);

        card_16 = view_.findViewById(R.id.card_16);
        card_16.setTag(IMAGE_VIEW_TAG);
        card_16.setOnLongClickListener(this);

        card_17 = view_.findViewById(R.id.card_17);
        card_17.setTag(IMAGE_VIEW_TAG);
        card_17.setOnLongClickListener(this);

        card_18 = view_.findViewById(R.id.card_18);
        card_18.setTag(IMAGE_VIEW_TAG);
        card_18.setOnLongClickListener(this);

        card_19 = view_.findViewById(R.id.card_19);
        card_19.setTag(IMAGE_VIEW_TAG);
        card_19.setOnLongClickListener(this);

        card_20= view_.findViewById(R.id.card_20);
        card_20.setTag(IMAGE_VIEW_TAG);
        card_20.setOnLongClickListener(this);

        card_21 = view_.findViewById(R.id.card_21);
        card_21.setTag(IMAGE_VIEW_TAG);
        card_21.setOnLongClickListener(this);

        card_22 = view_.findViewById(R.id.card_22);
        card_22.setTag(IMAGE_VIEW_TAG);
        card_22.setOnLongClickListener(this);

        content_card_1 = view_.findViewById(R.id.content_card_1);
        content_card_1.setOnDragListener(this);

//        content_card_2 =view_.findViewById(R.id.content_card_2);
//        content_card_2.setOnDragListener(this);
//
//        content_card_3 =view_.findViewById(R.id.content_card_3);
//        content_card_3.setOnDragListener(this);
//
//        content_card_4 =view_.findViewById(R.id.content_card_4);
//        content_card_4.setOnDragListener(this);
//
//        content_card_5 =view_.findViewById(R.id.content_card_5);
//        content_card_5.setOnDragListener(this);
    }

    @Override
    public boolean onDrag(View view, DragEvent event) {

        int action = event.getAction();

        switch (action) {
            case DragEvent.ACTION_DRAG_STARTED:
                if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {
                    return true;
                }
                return false;

            case DragEvent.ACTION_DRAG_ENTERED:
                view.getBackground().setColorFilter(Color.WHITE, PorterDuff.Mode.SRC_IN);
                view.invalidate();
                return true;

            case DragEvent.ACTION_DRAG_LOCATION:
                return true;

            case DragEvent.ACTION_DRAG_EXITED:
                view.getBackground().clearColorFilter();
                view.invalidate();
                return true;

            case DragEvent.ACTION_DROP:
                ClipData.Item item = event.getClipData().getItemAt(0);
//                String dragData = item.getText().toString();
//                Toast.makeText(getContext(), "Dragged data is " + dragData, Toast.LENGTH_SHORT).show();
                view.getBackground().clearColorFilter();
                view.invalidate();
                View v = (View) event.getLocalState();
                ViewGroup owner = (ViewGroup) v.getParent();
                owner.removeView(v);//remove the dragged view
                LinearLayout container = (LinearLayout) view;//caste the view into LinearLayout as our drag acceptable layout is LinearLayout
                container.addView(v);//Add the dragged view
                v.setVisibility(View.VISIBLE);//finally set Visibility to VISIBLE
                return true;

            case DragEvent.ACTION_DRAG_ENDED:
                view.getBackground().clearColorFilter();
                view.invalidate();
                if (event.getResult())

                    soltar_carta();
//                    Toast.makeText(getContext(), "Carta seleccionada", Toast.LENGTH_SHORT).show();
//                else
//                    Toast.makeText(getContext(), "The drop didn't work.", Toast.LENGTH_SHORT).show();
                return true;

            default:
                Log.e("DragDrop Example", "Unknown action type received by OnDragListener.");
                break;
        }
        return false;
    }

    @Override
    public boolean onLongClick(View view) {

        selected = view_.findViewById(view.getId());

        ClipData.Item item = new ClipData.Item((CharSequence) view.getTag());
        String[] mimeTypes = {ClipDescription.MIMETYPE_TEXT_PLAIN};
        ClipData data = new ClipData(view.getTag().toString(), mimeTypes, item);
        View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
        view.startDrag(data//data to be dragged
                , shadowBuilder //drag shadow
                , view//local data about the drag and drop operation
                , 0//no needed flags
        );
//        view.setVisibility(View.INVISIBLE);
        return true;
    }

    private void soltar_carta(){

        aleatorio = (int) (Math.random() * (arrayListCards.size()-1));

        action();

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(selected.getLayoutParams());
        params.setMargins(0, 0, 0, 0);
        selected.setLayoutParams(params);
        selected.setImageBitmap(arrayListCards.get(aleatorio));

        arrayListCards_selected.add(arrayListCards.get(aleatorio));
        names_cards_selected.add(names_cards.get(aleatorio));

        arrayListCards.remove(aleatorio);
        names_cards.remove(aleatorio);

    }

    private void action(){

        card++;
        TextView temp;

        switch (card){
            case 2:
                temp = view_.findViewById(R.id.arrastra_1);
                temp.setVisibility(View.GONE);
                temp = view_.findViewById(R.id.arrastra_2);
                temp.setVisibility(View.VISIBLE);
                temp = view_.findViewById(R.id.name_1);
                temp.setVisibility(View.VISIBLE);
                temp.setText(names_cards.get(aleatorio));
                content_card_1.setEnabled(false);
                content_card_2 = view_.findViewById(R.id.content_card_2);
                content_card_2.setOnDragListener(this);
                break;

            case 3:
                temp = view_.findViewById(R.id.arrastra_2);
                temp.setVisibility(View.GONE);
                temp = view_.findViewById(R.id.arrastra_3);
                temp.setVisibility(View.VISIBLE);
                temp = view_.findViewById(R.id.name_2);
                temp.setVisibility(View.VISIBLE);
                temp.setText(names_cards.get(aleatorio));
                content_card_2.setEnabled(false);
                content_card_3 =view_.findViewById(R.id.content_card_3);
                content_card_3.setOnDragListener(this);
                break;

            case 4:
                temp = view_.findViewById(R.id.arrastra_3);
                temp.setVisibility(View.GONE);
                temp = view_.findViewById(R.id.arrastra_4);
                temp.setVisibility(View.VISIBLE);
                temp = view_.findViewById(R.id.name_3);
                temp.setVisibility(View.VISIBLE);
                temp.setText(names_cards.get(aleatorio));
                content_card_3.setEnabled(false);
                content_card_4 =view_.findViewById(R.id.content_card_4);
                content_card_4.setOnDragListener(this);
                break;

            case 5:
                temp = view_.findViewById(R.id.arrastra_4);
                temp.setVisibility(View.GONE);
                temp = view_.findViewById(R.id.arrastra_5);
                temp.setVisibility(View.VISIBLE);
                temp = view_.findViewById(R.id.name_4);
                temp.setVisibility(View.VISIBLE);
                temp.setText(names_cards.get(aleatorio));
                content_card_4.setEnabled(false);
                content_card_5 =view_.findViewById(R.id.content_card_5);
                content_card_5.setOnDragListener(this);
                break;

            case 6:
                content_card_5.setEnabled(false);
                temp = view_.findViewById(R.id.arrastra_5);
                temp.setVisibility(View.GONE);
                temp = view_.findViewById(R.id.name_5);
                temp.setVisibility(View.VISIBLE);
                temp.setText(names_cards.get(aleatorio));
                LinearLayout linearLayout = view_.findViewById(R.id.cards);
                linearLayout.setVisibility(View.INVISIBLE);
                LinearLayout linearLayout2 = view_.findViewById(R.id.btn_result);
                linearLayout2.setVisibility(View.VISIBLE);
                break;
        }



    }

    private void generate_arrayListCards(){

        Bitmap bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_loco0);
        names_cards.add("El\nLoco");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_mago1);
        names_cards.add("El\nMago");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_sacerdotista2);
        names_cards.add("La\nSacerdotista");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_emperatriz3);
        names_cards.add("La\nEmperatriz");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_emperador4);
        names_cards.add("El\nEmperador");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_sumo_sacerdote5);
        names_cards.add("El\nSumo Sacerdote");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.los_enamorados6);
        names_cards.add("Los\nEnamorados");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_carro7);
        names_cards.add("El\nCarro");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_justicia8);
        names_cards.add("La\nJusticia");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.hermitano9);
        names_cards.add("Hermanito\n ");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.rueda_de_la_fortuna10);
        names_cards.add("Rueda de\nla fortuna");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_fuerza11);
        names_cards.add("La\nFuerza");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_colgado12);
        names_cards.add("El\nColgado");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_muerte13);
        names_cards.add("La\nMuerte");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_templaza14);
        names_cards.add("La\nTemplaza");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_diablo15);
        names_cards.add("El\nDiablo");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_torre16);
        names_cards.add("La\nTorre");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_estrella17);
        names_cards.add("La\nEstrella");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.la_luna18);
        names_cards.add("La\nLuna");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_sol19);
        names_cards.add("El\nSol");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_juicio20);
        names_cards.add("El\nJuicio");
        arrayListCards.add(bitmap);

        bitmap = BitmapFactory.decodeResource(getContext().getResources(), R.drawable.el_mundo21);
        names_cards.add("El\nMundo");
        arrayListCards.add(bitmap);

    }

    private void button_next(){
        Button btn = view_.findViewById(R.id.btn_next);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new ResultadoFragment(arrayListCards_selected,names_cards_selected);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment).commitNow();

//                Fragment fragment = new ResultadoFragment(arrayListCards_selected,names_cards_selected);
//                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//                fragmentTransaction.replace(R.id.frame_content, fragment);
//                fragmentTransaction.addToBackStack(null);
//                fragmentTransaction.commit();
            }
        });
    }

}
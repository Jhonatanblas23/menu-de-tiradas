package com.marbookgroup.tarot_example;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;

public class MainActivity_5cards extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_5cards);

        if (savedInstanceState == null) {
            Fragment fragment = new CaminoFragment();
            getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, fragment).commitNow();
        }

    }
}
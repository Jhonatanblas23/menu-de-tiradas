package com.marbookgroup.tarot_example;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class AdapterItemResult extends RecyclerView.Adapter<AdapterItemResult.MyViewHolder>  {

    private ArrayList<Bitmap> arrayListCards_selected;
    private ArrayList<String> names_cards_selected;

    public AdapterItemResult(ArrayList<Bitmap> arrayListCards_selected, ArrayList<String> names_cards_selected) {
        this.arrayListCards_selected = arrayListCards_selected;
        this.names_cards_selected = names_cards_selected;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder{

        //Variables del item
        TextView name_card;
        ImageView image_card;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            name_card = itemView.findViewById(R.id.name_card);
            image_card = itemView.findViewById(R.id.image_card);
        }
    }

    @NonNull
    @Override
    public AdapterItemResult.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_result,null,false);
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        view.setLayoutParams(lp);
        return new AdapterItemResult.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AdapterItemResult.MyViewHolder holder, int position) {
        holder.image_card.setImageBitmap(arrayListCards_selected.get(position));
        holder.name_card.setText(names_cards_selected.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayListCards_selected.size();
    }
}
